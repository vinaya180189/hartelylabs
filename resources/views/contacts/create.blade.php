@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Contact
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('contacts.store') }}">
          <div class="form-group">
              @csrf
              <label for="first_name">First Name:</label>
              <input type="text" class="form-control" name="first_name"/>
          </div>
		  <div class="form-group">
              <label for="middle_name">Middle Name :</label>
              <input type="text" class="form-control" name="middle_name"/>
          </div>
          <div class="form-group">
              <label for="last_name">Last Name :</label>
              <input type="text" class="form-control" name="last_name"/>
          </div>
          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email_address"/>
          </div>
		  <div class="form-group">
              <label for="mobile">Mobile:</label>
              <input type="text" class="form-control" name="mobile"/>
          </div>
		  <div class="form-group">
              <label for="secondary_contact">Secondary Contact:</label>
              <input type="text" class="form-control" name="secondary_contact"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection