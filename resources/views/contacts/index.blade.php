@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
			<th>Name</th>
			<th>Email</th>
			<th>Mobile</th>
			<th>Secondary Contact</th>
			<th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($userContacts as $contact)
        <tr>
            <td>{{$contact->first_name}} {{$contact->last_name}}</td>
            <td>{{$contact->email_address}}</td>
            <td>{{$contact->mobile}}</td>
			<td>{{$contact->secondary_contact}}</td>
            <td><a "{{action('UserContactsController@edit',  ['id' => $contact->id])}}" class="btn btn-primary">Edit</a></td>
            <td>
			
                <form action="{{action('UserContactsController@destroy',  ['id' => $contact->id])}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection