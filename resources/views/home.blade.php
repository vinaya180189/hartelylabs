@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					<Table width="100%">
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Secondary Contact</th>
						</tr>
						<tr>
							<td>Name</td>
							<td>Email</td>
							<td>Mobile</td>
							<td>Secondary Contact</td>
						</tr>
						
					</Table>
                    You are logged in!
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
