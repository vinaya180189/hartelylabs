-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 01:28 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(3) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `secondary_contact` varchar(20) NOT NULL,
  `photo` blob NOT NULL,
  `password` varchar(400) NOT NULL,
  `activation_key` varchar(400) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `remember_token` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `mobile`, `secondary_contact`, `photo`, `password`, `activation_key`, `status`, `updated_at`, `created_at`, `remember_token`) VALUES
(1, 'VINAYA', 'MAYUR', 'PARASHAR', 'vinaya180189@gmail.com', '9860310077', '9730763970', 0x4453435f303033332e4a5047, '$2y$10$uHxjv5m6JGO/3lUOGupZMuD6sYBDC3iMGCTaCa.FnWbeVlErG6ZAi', '0293220a6cb74a51358b685df195e3ff73db78de3adf1285bfc7fdd2ce646ee2', 'active', '2019-02-04 16:18:27', '2019-02-04 16:18:27', 'cXgFzmKmBOI1ypn71y51Q6EntFeTNFm86tQvsEiowDWbuR6W4BOMhXnvRVjB'),
(2, 'Mayur', 'Kishor', 'Parashar', 'mayur.parashar1@gmail.com', '9730763970', '9860310077', 0x4453435f303033332e4a5047, '$2y$10$lXANnzHD.lDdB4Z8mc5LTO8WkrMf2zc8dcumWP1CbswkB2UV.hYHW', '5499e650374d64b91f34629758c47538d18ec635e3f541a229960ee2966a6690', 'active', '2019-02-04 16:21:01', '2019-02-04 16:21:01', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `contact_id` int(3) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email_address` varchar(40) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `secondary_contact` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_contacts`
--

INSERT INTO `user_contacts` (`contact_id`, `first_name`, `middle_name`, `last_name`, `email_address`, `mobile`, `secondary_contact`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Kishor', 'Girdhar', 'Parashar', 'kishor@parashar.com', '9876543210', '2312342345', 1, '2019-02-06 04:14:33', '2019-02-06 04:14:33'),
(2, 'PIYUSH', 'KISHOR', 'PARASHAR', 'piyush@gmail.com', '2321344345', '2334423423', 1, '2019-02-06 11:55:18', '2019-02-06 11:55:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `contact_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
