<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\UserContacts;
use Auth;


class UserContactsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
		if (Auth::check())
		{
			$id = Auth::user()->id;
			
			$userContacts = UserContacts::where('user_id',$id)->get();

			return view('contacts.index', compact('userContacts'));
		} else {
			return view('welcome');
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return view('contacts.create');
    }

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		return Validator::make($data, [
            'first_name'=>'required|string',
			'last_name'=> 'required|string',
			'email_address' => 'required|string|email|max:255',
            'mobile' => 'required|min:10|max:13',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validator($request->all())->validate();
      
		$userContact = new UserContacts([
        'first_name' => $request->get('first_name'),
		'middle_name' => $request->get('middle_name'),
		'last_name' => $request->get('last_name'),
        'email_address'=> $request->get('email_address'),
        'mobile'=> $request->get('mobile'),
		'secondary_contact' => $request->get('secondary_contact'),
		'user_id' => $request->user()->id,
      ]);
	  
      $userContact->save();
      return redirect('/contacts')->with('success', 'contact has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$userContacts = UserContacts::find($id);

        return view('contacts.edit', compact('userContacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all())->validate();

		$usercontact = UserContacts::find($id);

		$usercontact->first_name = $request->get('first_name');
		$usercontact->middle_name = $request->get('middle_name');
		$usercontact->last_name = $request->get('last_name');
        $usercontact->email_address= $request->get('email_address');
        $usercontact->mobile= $request->get('mobile');
		$usercontact->secondary_contact = $request->get('secondary_contact');

		$usercontact->save();

      return redirect('/contacts')->with('success', 'contact has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		
		$usercontact = UserContacts::find($id);
		$usercontact->delete();

		return redirect('/contacts')->with('success', 'Contact has been deleted Successfully');
    }
}