<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContacts extends Model
{
    //
	
	protected $table = 'user_contacts';
	
	protected $fillable = [
        'first_name','middle_name','last_name', 'email_address', 'mobile','secondary_contact', 'user_id',
    ];
}
